Hello $foo,

As announced XXX weeks ago, I have gone over your packages orphaning
them.

However I still need some indication on how we should proceed with your
Debian account — after the orphaning your account is technically idle,
and an idle account poses a security risk for the project.

Let me tell you about the options, giving them in the MIA team's order
of preference.

The best thing of course would be if you could get active again.  This
would allow us to close the MIA process for good and let you retain
your account.

Otherwise, retirement would be an option to consider.  This has the
advantage of providing a graceful exit while allowing for an easier
return if you so desire in the future.  If retirement could be an
option for you, let me know, and I can provide pointers (but I hope
you go for option one).

The least preferable option would be for you not to answer at all.  This
will eventually lead to a where-art-thou (WAT) ping; if you also leave
this ping unanswered, your account will be closed by the Debian Account
Manager, removing you from the project.  Note that to come back after
this happens you will need to go through a full-length NM process.

Please be assured that the MIA team is not asking you to rush to any
decision right now, but it would be great if you could answer within
the next three months.

You are always very welcome to resume contributing at any time, even if
you go for option two or three.

Thank you for all your past and hopefully future contributions to Debian!

Regards,
