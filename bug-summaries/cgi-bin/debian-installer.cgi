#! /usr/bin/perl -Tw

require '/srv/bugs.qa.debian.org/perl/bug-summary.pl';

use CGI qw(:standard);

my $dipkgs = '/srv/bugs.qa.debian.org/data/lists/debian-installer';
my %dipkgs = map { binary_to_source($_) => 1 } read_package_list ($dipkgs);
my @dipkgs = sort keys %dipkgs;

html_header ('Bugs in debian-installer');
show_table (@dipkgs);
html_footer ();
