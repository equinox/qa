#!/usr/bin/perl -w

#    extract_dm.pl - read data from https://ftp-master.debian.org/dm.txt for use in ddpo
#    Copyright (C) 2012 Bart Martens <bartm@knars.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use DB_File;

my $db_filename = "dm-new.db";
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
	or die "Can't open database $db_filename : $!";

my $block = "";
my %dd_packages;

my $dir = '/etc/ssl/ca-debian';
my $ca_debian = -d $dir ? "--ca-directory=$dir" : '';

open INPUT, "wget $ca_debian --timeout=15 --tries=1 -q -O - https://ftp-master.debian.org/dm.txt |" or die "wget: $!";
while(<INPUT>)
{
	chomp;

	$block .= "$_\n";

	if( /^$/ )
	{
		$block =~ s/\n / /gs;

		my $fingerprint = undef;
		my $allow = undef;

		foreach my $line ( split /\n/, $block )
		{
			$fingerprint = $1 if( $line =~ /^Fingerprint: (.*)/ );
			$allow = $1 if( $line =~ /^Allow: (.*)/ );
		}

		$allow =~ s/, /,/g;

		foreach my $allow_entry ( split /,/, $allow )
		{
			$allow_entry =~ /^(\S+) \(([A-F0-9]+)\)$/ or next;
			my $package = $1;
			my $fingerprint_dd = $2;

			$db{"pf:$package:$fingerprint"} = 1;

			$dd_packages{$fingerprint_dd} = "" if( ! defined $dd_packages{$fingerprint_dd} );
			$dd_packages{$fingerprint_dd} .= " $package";
		}

		$block = "";
	}
}
close INPUT;

die "low number of keys: ".keys(%dd_packages)." ".keys(%db) if( keys(%dd_packages) < 50 or keys(%db) < 330 ); # 82 549 on 2012-12-01

foreach my $fingerprint_dd ( keys %dd_packages )
{
	$dd_packages{$fingerprint_dd} =~ s/^ +//;
	$db{"ddp:$fingerprint_dd"} = $dd_packages{$fingerprint_dd};
}

